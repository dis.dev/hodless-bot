"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Spoilers from "./components/spoilers.js";


// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')


// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

if (document.querySelector('[data-play]')) {
    const playButton = document.querySelector('[data-play]')
    const playTarget = playButton.dataset.play
    const player = new Audio(playTarget);
    let playing = false;
    player.preload = 'auto';
    player.addEventListener('ended', function(){ // слушаем окончание трека
        playing = false;
    })

    playButton.addEventListener("dblclick", (event) => {
        console.log(playTarget)
        playPause()
    });

    function playPause() {
        if( playing) {
            player.pause();
        } else {
            player.play();
        }
        playing = !playing;
    }
}

